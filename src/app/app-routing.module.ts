import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaginationTableComponent } from './pagination-table/pagination-table.component';
import { UserDetailsComponent } from './user-details/user-details.component';

const routes: Routes = [
  {
    path: '',
    component: PaginationTableComponent,
  },
  { path: 'user/:userId', component: UserDetailsComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
