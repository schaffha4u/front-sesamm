import { Pipe, PipeTransform } from '@angular/core';
import { Address } from '../models/address.model';

@Pipe({
  name: 'concatenateAddress',
})
/**
 * Pipe to concatenate the address
 */
export class ConcatenateAddressPipe implements PipeTransform {
  /**
   * Concatenate the address of the user
   *
   * @param value: Address - the Address
   * @param args
   * @returns The concatenated address
   */
  transform(value: Address, ...args: unknown[]): string {
    return (
      value.streetNumber +
      ' ' +
      value.street +
      ', ' +
      value.postalCode +
      ' ' +
      value.city
    );
  }
}
