import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PaginatedUsers } from '../models/paginated-users.model';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  readonly API_URL: string = 'http://localhost:5500/';

  constructor(private httpClient: HttpClient) {}

  /**
   * Get the list of users with pagination
   *
   * @param pageSize: number - the number of users per page
   * @param pageNumber: number - the number of the page to get
   * @returns paginatedUsers - the list of users with pagination
   */
  public getAllUsers(
    pageSize: string,
    pageNumber: string
  ): Observable<PaginatedUsers> {
    let httpParams: HttpParams = new HttpParams({
      fromObject: {
        pageSize: pageSize,
        pageNumber: pageNumber,
      },
    });
    return this.httpClient.get<PaginatedUsers>(`${this.API_URL}users`, {
      params: httpParams,
    });
  }

  /**
   * Get the user with the given id
   *
   * @param id: number - the id of the user to get
   * @returns user
   */
  public getUser(id: number): Observable<User> {
    return this.httpClient.get<User>(`${this.API_URL}user/${id}`);
  }
}
