import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../models/user.model';
import { UserService } from '../services/user.service';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { PaginatedUsers } from '../models/paginated-users.model';

@Component({
  selector: 'app-pagination-table',
  templateUrl: './pagination-table.component.html',
  styleUrls: ['./pagination-table.component.css'],
})
export class PaginationTableComponent implements OnInit {
  /* The list of users to print */
  userList: User[] = [];
  /* The list of columns in the table */
  displayedColumns: string[] = [
    'id',
    'lastName',
    'firstName',
    'gender',
    'age',
    'email',
    'address',
  ];
  dataSource: MatTableDataSource<User>;
  @ViewChild('paginator')
  paginator: MatPaginator;
  /*@ViewChild(MatSort) set matSort(sort: MatSort) {
    if (!this.dataSource.sort) {
      this.dataSource.sort = sort;
    }
  }*/
  @ViewChild('userTbSort') userTbSort: MatSort = new MatSort();
  /* Availables page sizes */
  pageSizes = [15, 30, 50];
  /* Stores current total number of users */
  totalElements: number = 0;

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit(): void {
    this.getUsers('15', '0');
    this.dataSource.paginator = this.paginator;
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.userTbSort;
  }

  /**
   * Redirects to the user details page
   *
   * @param row: row of the table clicked
   */
  navigateToUserDetails(row: any): void {
    this.router.navigateByUrl(`/user/${row.id}`, { state: { userId: row.id } });
  }

  /**
   * Reload the users list when new page size or page number is selected
   *
   * @param event: PageEvent emitted by the paginator
   */
  nextPage(event: PageEvent): void {
    const newPageSize: string = event.pageSize.toString();
    const newPageNumber: string = event.pageIndex.toString();
    this.getUsers(newPageSize, newPageNumber);
  }

  /**
   * Gets the users list from the server
   *
   * @param pageSize: string number of users per page
   * @param pageNumber: string number of the page to get
   */
  private getUsers(pageSize: string, pageNumber: string): void {
    this.userService
      .getAllUsers(pageSize, pageNumber)
      .subscribe((response: PaginatedUsers) => {
        this.userList = response.rows;
        this.totalElements = response.count;
        this.dataSource = new MatTableDataSource(this.userList);
        this.dataSource.sort = this.userTbSort;
      });
  }
}
