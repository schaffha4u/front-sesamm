import { HttpParams } from '@angular/common/http';
import { Component, ComponentFactoryResolver, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from '../models/user.model';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css'],
})
export class UserDetailsComponent implements OnInit {
  /* Id of the user */
  userId: number;
  /* Id of the user entered in the url */
  paramUserId: number;
  /* User to print, associated to userId */
  user: User;
  /** Defines is user asked to print with userId exists or not,
   * and decides wich page to show
   */
  userExists: boolean = true;
  /* URL to the picture of the use */
  userPictureURL: string;
  hasLoaded: boolean = false;

  constructor(private route: ActivatedRoute, private userService: UserService) {
    let paramUserId = this.route.snapshot?.paramMap?.get('userId');
    if (!paramUserId) this.userExists = false;
    else this.userId = parseInt(paramUserId);
  }

  ngOnInit(): void {
    if (this.userExists)
      this.userService.getUser(this.userId).subscribe(
        (user: User) => {
          this.user = user;
          this.userPictureURL = `/assets/${this.user.gender}.jpg`;
          this.hasLoaded = true;
        },
        (err) => {
          this.userExists = false;
          this.hasLoaded = true;
        }
      );
  }
}
