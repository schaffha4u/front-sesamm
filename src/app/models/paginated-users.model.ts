import { User } from './user.model';

/**
 * Define the PaginatedUsers DAO
 * @param rows: User[] - the list of users
 * @param count: number - the total number of users
 */
export class PaginatedUsers {
  constructor(public count: number, public rows: User[]) {}
}
