/**
 * Define the Address model
 */
export class Address {
  constructor(
    public id: number,
    public streetNumber: number,
    public street: string,
    public postalCode: string,
    public city: string
  ) {}
}
