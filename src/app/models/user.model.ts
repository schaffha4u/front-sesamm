import { Address } from './address.model';

/**
 * Define the User model
 */
export class User {
  constructor(
    public id: number,
    public firstName: string,
    public lastName: string,
    public gender: string,
    public email: string,
    public age: number,
    public eyeColor: string,
    public phone: string,
    public registered: string,
    public addresses: Address[]
  ) {}
}
