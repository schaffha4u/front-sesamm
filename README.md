
# Front SESAMm

Front-end application for a technical test in order to get an apprentice at SESAMm (Metz) for the Master 2 GI at Metz. 
It has been realised between 05/28/2022 and 05/29/2022.

## Tech Stack

**Client:** AngularJS

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/schaffha4U/front-sesamm.git
```
Go to the project directory
```bash
  cd front-sesamm
```
Install dependencies

```bash
  npm install
```

Start the server

```bash
  ng serve
```

## Authors

- Bastien SCHAFFHAUSER

